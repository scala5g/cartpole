import unittest
from random import random

import torch

from neural_network_.NeuralNetwork import NeuralNetwork


class TestNeuralNetwork(unittest.TestCase):
    """
    Here is the thing: Due to Neural Network code is almost all from pytorch
    is no sense to test a library of another developer, this is a thin line that
    should not be crossed
    """
    number_of_outputs = 2

    @classmethod
    def setUpClass(cls) -> None:
        number_of_inputs = 3

        hidden_layer = number_of_inputs * 2

        cls.neural_network = NeuralNetwork(number_of_inputs,
                                           hidden_layer,
                                           cls.number_of_outputs
                                           )

    def test_forward_on_nn(self):
        network_input = torch.tensor((random(), random(), random()))
        output = self.neural_network.forward(network_input)

        self.assertEqual(len(output), self.number_of_outputs)


if __name__ == '__main__':
    unittest.main()
