import unittest

import numpy as np

from dqn_training.train_video_server_dqn import main, solved_condition, get_average_steps_from_report_window, \
    get_steps_window


class TestTrainVideoServerDQN(unittest.TestCase):

    def test_dqn_agent_converges(self):
        steps_total = main(False, solve_without_minimum_episodes=True)
        steps_window = get_steps_window(10, 0)
        av_steps_from_report_window = get_average_steps_from_report_window(steps_total, steps_window)
        environment_is_solved = solved_condition(av_steps_from_report_window, len(np.arange(0, 24 * 2, 0.166)))
        self.assertTrue(environment_is_solved)


if __name__ == '__main__':
    unittest.main()
