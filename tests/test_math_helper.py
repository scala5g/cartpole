import unittest

from math_helpers.epsilon_decay import calculate_epsilon
from tune_parameters import egreedy_final, egreedy
# import matplotlib.pyplot as plt


class TestCalculateEpsilonDecay(unittest.TestCase):
    epsilon_decay_arr = []

    @classmethod
    def setUpClass(cls):
        print("Starting all the tests.")
        num_episodes = 999999
        for step in range(num_episodes):
            cls.epsilon_decay_arr.append(calculate_epsilon(step))

    def test_big_number_of_episodes(self):
        """
        given a big number of episodes
        when epsilon decay calc finish
        then last epsilon decay number is greater/equal than egreedy final
        """

        # plt.plot(range(num_episodes), epsilon_decay)
        # plt.show()
        self.assertTrue(self.epsilon_decay_arr[-1] >= egreedy_final)

    def test_small_number_of_episodes(self):
        """
        given a big number of episodes
        when epsilon decay calc finish
        then first epsilon decay number is lower/equal than egreedy final
        """

        self.assertTrue(self.epsilon_decay_arr[0] <= egreedy)



if __name__ == '__main__':
    unittest.main()
