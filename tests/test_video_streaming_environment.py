import unittest

import numpy as np
from matplotlib import pyplot as plt

from dqn_5g_environment.VideoStreamingEnvironment import VideoStreamingEnvironment, generate_traffic, _reward_function, \
    generate_test_traffic
from legacy.train_dqn_legacy import QNet_Agent
from plotting.dqn_plot import plot_video_server_dqn_behaviour, plot_steps, plot_sla_percentage


class TestVideoStreamingEnvironment(unittest.TestCase):

    def test_observation_action_space(self):
        video_env = VideoStreamingEnvironment()
        states = video_env.observation_space.shape
        actions = video_env.action_space
        self.assertEqual(states[0], 2)
        self.assertEqual(actions.n, 3)

    def test_increase_server_action(self):
        video_env = VideoStreamingEnvironment()
        previous_server_quantity = 3
        max_server_quantity = 10
        video_env.current_number_of_servers = previous_server_quantity
        video_env.max_number_of_servers = max_server_quantity
        video_env.step(VideoStreamingEnvironment.scale_up_servers)
        self.assertEqual(previous_server_quantity + 1, video_env.current_number_of_servers)

    def test_decrease_server_action(self):
        video_env = VideoStreamingEnvironment()
        previous_server_quantity = 3
        max_server_quantity = 10
        video_env.current_number_of_servers = previous_server_quantity
        video_env.max_number_of_servers = max_server_quantity
        video_env.step(VideoStreamingEnvironment.scale_down_servers)
        self.assertEqual(previous_server_quantity - 1, video_env.current_number_of_servers)

    def test_invalid_action(self):
        with self.assertRaises(Exception) as context:
            video_env = VideoStreamingEnvironment()
            max_action = video_env.action_space.n
            video_env._take_action(max_action + 1)
            self.assertTrue('Invalid action' in context.exception)

    def test_current_transition(self):
        video_env = VideoStreamingEnvironment()
        steps = 400
        video_env.traffic = range(0, steps)
        video_env.transition_threshold = 5
        video_env.max_number_of_servers = 100000

        for i in range(steps):
            state, reward, done, _ = video_env.step(action=VideoStreamingEnvironment.scale_up_servers)

        self.assertEqual(video_env.current_number_of_servers,
                         steps // (video_env.transition_threshold + 1) + 2)

    def test_reset_env(self):
        global state
        video_env = VideoStreamingEnvironment()
        steps = 400
        video_env.traffic = range(0, steps)
        video_env.transition_threshold = 5
        video_env.max_number_of_servers = 100000

        for i in range(steps):
            state, reward, done, _ = video_env.step(action=1)
        self.assertNotEqual(video_env.state[0], 0)
        self.assertNotEqual(video_env.current_number_of_servers, 0)
        video_env.reset()
        self.assertNotEqual(video_env.state, None)
        self.assertEqual(video_env.current_number_of_servers,
                         VideoStreamingEnvironment.initial_number_of_servers)

    def test_render_mode(self):
        video_env = VideoStreamingEnvironment()
        self.assertEqual(video_env.render(), None)

    def _test_generate_graph_traffic_time(self):
        min_value = 0
        max_value = 24
        # time = np.arange(min_value, max_value, 0.016)
        traffic = generate_test_traffic()
        time = len(traffic)
        fig, ax = plt.subplots()
        plt.scatter(range(time), traffic, s=2)
        max_value = time
        major_ticks = np.arange(min_value, max_value, max_value / 20)
        minor_ticks = np.arange(min_value, max_value, max_value / 40)
        ax.set_xticks(major_ticks)
        ax.set_xticks(minor_ticks, minor=True)
        ax.grid(which='both')
        fig.suptitle('Traffic on Jan. 1th', fontsize=20)
        plt.xlabel('Time (Minutes)', fontsize=18)
        plt.ylabel('Active connections', fontsize=16)
        plt.show()

    def _test_generate_graph_traffic_reward(self):
        min_value = 0
        max_value = 24
        time = np.arange(min_value, max_value, 0.016)
        traffic = generate_traffic(time)
        videoEnv = VideoStreamingEnvironment()
        delay = videoEnv._server_delay(traffic / 1)
        reward = []
        for de in delay:
            reward.append(_reward_function(de))
        fig, reward_plot = plt.subplots()

        reward_plot.scatter(range(len(traffic)), reward, color="b", s=1)
        reward_plot.set_ylabel('Reward', color='b')
        reward_plot.grid(which='both')
        reward_plot.set_xlabel('Time (Minutes)')
        delay_plot = reward_plot.twinx()
        delay_plot.scatter(range(len(traffic)), delay, color="r", s=1)
        delay_plot.set_ylabel('Delay (ms)', color='r')
        delay_plot.tick_params('y', colors='r')
        fig.suptitle('Traffic on Jan. 1th, 1 server on', fontsize=20)
        plt.show()

    def _test_generate_graph_threshold_scaling(self):
        videoEnv = VideoStreamingEnvironment()

        done = False
        rewards = []
        server_numbers = []
        delays = []
        previous_state = 0
        while not done:
            if previous_state > 310:  # TODO this is not penalizing the delay of scaling to another instance.
                action = 1
            elif previous_state < 295:
                action = 2
            else:
                action = 0
            state, reward, done, other = videoEnv.step(action)

            rewards.append(reward)
            server_numbers.append(videoEnv.current_number_of_servers)
            delays.append(state[0])
            previous_state = state[0]

        fig, server_plot = plt.subplots()

        server_plot.plot(range(len(server_numbers)), server_numbers)
        delay_plot = server_plot.twinx()
        delays.remove(max(delays))
        traffic = videoEnv.traffic
        delay_plot.scatter(range(len(traffic)), traffic, color="r", s=1)
        plt.show()

    def _test_generate_graph_reward(self):
        min_value = 0
        max_value = 1
        delay = np.arange(min_value, max_value, 0.02)
        videoEnv = VideoStreamingEnvironment()
        rewards = []
        for current_delay in delay:
            rewards.append(_reward_function(current_delay))
        fig, ax = plt.subplots()
        major_ticks = np.arange(min_value, max_value, 5)
        minor_ticks = np.arange(min_value, max_value, 2.5)
        ax.set_xticks(major_ticks)
        ax.set_xticks(minor_ticks, minor=True)
        ax.grid(which='both')
        fig.suptitle('Reward function', fontsize=20)
        plt.xlabel('Delay (ms)', fontsize=18)
        plt.ylabel('Reward', fontsize=16)

        plt.scatter(delay, rewards)
        plt.show()

    def _test_generate_graph_delay_server(self):
        min_value = 0
        max_value = 60
        users = np.arange(min_value, max_value, 1)
        videoEnv = VideoStreamingEnvironment()

        server_delay_linear = []
        server_delay_gauss = []
        for user in users:
            server_delay_linear.append(videoEnv._server_delay(user))
            server_delay_gauss.append(videoEnv._server_delay_(user))
        fig, ax = plt.subplots()
        plot_gauss = ax.twinx()
        plot_gauss.scatter(users, server_delay_gauss, s=4, color="r")
        ax.scatter(users, server_delay_linear, s=4, color="b")
        major_ticks = np.arange(min_value, max_value, 5)
        minor_ticks = np.arange(min_value, max_value, 2.5)

        ax.set_xticks(major_ticks)
        ax.set_xticks(minor_ticks, minor=True)
        ax.grid(which='both')
        ax.set_ylim(0, 0.6)
        plot_gauss.set_ylim(0, 0.6)
        fig.suptitle('Delay per server', fontsize=20)
        plt.xlabel('Number of users', fontsize=18)
        plt.ylabel('Delay (ms)', fontsize=16)
        # ax.set_yticks(y_major_ticks)
        # ax.set_yticks(y_minor_ticks, minor=True)
        plt.show()

    def _test_plot_video_server_dqn_behaviour(self):
        agent = QNet_Agent()
        env = VideoStreamingEnvironment()
        plot_video_server_dqn_behaviour(env, agent, False)

    def _test_plot_steps(self):
        _, steps_plot = plt.subplots()
        agent_steps_ = []
        agent_legacy_steps_ = []
        for i in range(2000):
            agent_steps_.append(i)
            agent_legacy_steps_.append(i)
        for i in range(2000, 4000):
            agent_steps_.append(i)
        plot_steps(steps_plot, agent_steps_, agent_legacy_steps_)
        plt.show()

    def _test_plot_steps(self):
        _, sla_plot = plt.subplots()
        delay_agent = [10, 19, 18, 20, 11, 50, 9]
        delay_legacy = [2, 129, 118, 20, 11, 50, 9]
        upper_bound = 20
        lower_bound = 10

        delays_5g_agent = []
        delays_legacy_agent = []
        for _ in range(5):
            delays_5g_agent.append(np.random.randint(lower_bound - 4, upper_bound + 10, size=20))
            delays_legacy_agent.append(np.random.randint(lower_bound - 4, upper_bound + 10, size=20))
        plot_sla_percentage(sla_plot, delays_5g_agent, delays_legacy_agent, upper_bound, lower_bound)
        plt.show()

if __name__ == '__main__':
    unittest.main()
