import random
import unittest

from dqn_improvers.ExperienceReplay import ExperienceReplay


class TestMemoryReplay(unittest.TestCase):
    memory_capacity_size = 200
    number_of_inputs = memory_capacity_size * 2
    memory_replay = ExperienceReplay(memory_capacity_size)
    batches = []

    @classmethod
    def setUpClass(cls) -> None:
        for _ in range(cls.number_of_inputs):
            new_batch = cls.get_new_batch()
            cls.memory_replay.push(*new_batch)

    @staticmethod
    def get_new_batch():
        return ("some_state", random.randint(0, 10),
                random.randint(0, 10),
                random.random(), False)

    def test_memory_size(self):
        """
        given a memory replay capacity size
        when the number of inputs is bigger than the capacity size
        then the memory replay maintains the same size
        """

        self.assertEquals(len(self.memory_replay), self.memory_capacity_size)

    def test_memory_sample_size(self):
        """
        given a memory replay
        when the number of inputs is bigger than the capacity size
        then the memory replay maintains the same size
        """

        batch_size = self.memory_capacity_size // 10
        batch = self.memory_replay.sample(batch_size)
        batch_list = list(batch)
        batch_item = random.sample(batch_list, 1)[0]
        self.assertEquals(len(batch_item), batch_size)

    def test_memory_content(self):
        """
        given a memory replay
        when a sample is picked
        then the sample contains elements from the previous input data
        """
        memory_capacity = 10
        memory_replay = ExperienceReplay(memory_capacity)
        all_data = []
        row = 3  # rewards

        for _ in range(memory_capacity * 2):
            batch = self.get_new_batch()
            all_data.append(batch)
            memory_replay.push(*batch)

        batch_size = 5
        batch = memory_replay.sample(batch_size)
        batch_list = list(batch)
        batch_rewards = batch_list[row]
        all_data_rewards = [data_element[row] for data_element in all_data]
        self.assertTrue(set(batch_rewards).issubset(set(all_data_rewards)))


if __name__ == '__main__':
    unittest.main()
