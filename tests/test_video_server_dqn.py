import unittest

from dqn_training.train_video_server_dqn import extra_bonus, solved_condition, get_average_steps_from_report_window, \
    get_episode_reward, get_steps_window
from plotting.dqn_plot import calculate_sla_ratio


class TestTrainVideoServerDQN(unittest.TestCase):

    def test_extra_bonus_when_environment_is_not_done_then_no_reward(self):
        done = False
        max_steps = 100
        reward = 10
        step = 99
        new_reward = extra_bonus(step, max_steps, done, reward)
        self.assertEqual(reward, new_reward)

    def test_extra_bonus_when_environment_is_done_and_makes_almost_all_steps_then_reward(self):
        done = True
        max_steps = 100
        reward = 10
        step = 99
        new_reward = extra_bonus(step, max_steps, done, reward)
        self.assertEqual(reward + 65, new_reward)

    def test_extra_bonus_when_environment_is_done_and_makes_almost_none_steps_then_no_reward(self):
        done = True
        max_steps = 100
        reward = 10
        step = 10
        new_reward = extra_bonus(step, max_steps, done, reward)
        self.assertEqual(reward, new_reward)

    def test_solved_condition_when_enough_steps_then_environment_is_solved(self):
        av_steps_from_report_window = 196
        max_steps = 200
        is_solved = solved_condition(av_steps_from_report_window, max_steps)
        self.assertTrue(is_solved)

    def test_solved_condition_when_not_enough_steps_then_environment_is_not_solved(self):
        av_steps_from_report_window = 100
        max_steps = 200
        is_solved = solved_condition(av_steps_from_report_window, max_steps)
        self.assertFalse(is_solved)

    def test_get_average_steps_from_report_window_when_steps_total_not_empty(self):
        value = 50
        steps_total = [value for _ in range(100)]
        report_window = 20
        average_steps = get_average_steps_from_report_window(steps_total, report_window)
        self.assertEqual(average_steps, value)

    def test_get_episode_reward_when_env_reward_not_empty(self):
        reward = 50
        env_reward = [reward for _ in range(100)]
        episode_step = 20
        average_reward = get_episode_reward(env_reward, episode_step)
        self.assertEqual(average_reward, reward)

    def test_get_steps_window(self):
        report_interval = 100
        margin = 30
        steps_window = get_steps_window(report_interval, margin)
        self.assertTrue(steps_window > report_interval)

    def test_get_sla_ratio(self):
        delay = [10, 19, 18, 20, 11, 50, 9]
        upper_bound = 20
        lower_bound = 10
        sla_ratio = calculate_sla_ratio(delay, upper_bound, lower_bound)
        self.assertEqual(sla_ratio, 5 / 7)


if __name__ == '__main__':
    unittest.main()
