import numpy as np
import torch
from matplotlib import pyplot as plt
from torch import Tensor

from dqn_5g_environment.VideoStreamingEnvironment import VideoStreamingEnvironment, \
    generate_traffic
from legacy.train_dqn_legacy import train_legacy_agent

threshold_color = "b"
agent_color = "r"


def environment_with_legacy_agent(env, legacy_agent, test_traffic):
    legacy_agent_delay = []
    legacy_agent_server_number = []
    for user_number in test_traffic:
        legacy_agent_server_number.append(env.current_number_of_servers)
        delay = env._server_delay(user_number)
        legacy_nn_action = legacy_agent.nn(Tensor([delay, env.current_number_of_servers])).detach()
        action = torch.max(legacy_nn_action, 0)[1]
        env._take_action(int(action))
        legacy_agent_delay.append(delay)
    env.reset()
    return legacy_agent_delay, legacy_agent_server_number


def benchmark_agent(env, current_agent, test_traffic):
    delay_agent = []
    server_number_agent = []
    for user_number in test_traffic:
        server_number_agent.append(env.current_number_of_servers)
        delay = env._server_delay(user_number)
        nn_action = current_agent.nn(Tensor([delay, env.current_number_of_servers])).detach()
        action = torch.max(nn_action, 0)[1]
        env._take_action(int(action))
        delay_agent.append(delay)
    return delay_agent, server_number_agent


def plot_video_server_dqn_behaviour(env, scala_5g_agent, agent_steps_, graph_previous_agents=False):
    if graph_previous_agents:
        legacy_agent, agent_legacy_steps_ = train_legacy_agent()

    env.current_number_of_servers = 1
    test_traffic_quantity = 5
    tests_traffic = []

    legacy_agent_delays = []
    legacy_agent_server_numbers = []

    scala_5g_agent_delays = []
    scala_5g_agent_server_numbers = []
    for _ in range(test_traffic_quantity):
        current_traffic = generate_traffic()
        tests_traffic.append(current_traffic)
        legacy_agent_delay, legacy_agent_server_number = benchmark_agent(env, legacy_agent,
                                                                         current_traffic)
        legacy_agent_server_numbers.append(legacy_agent_server_number)
        legacy_agent_delays.append(legacy_agent_delay)

        scala_5g_delay, scala_5g_server_number = benchmark_agent(env, scala_5g_agent, current_traffic)
        scala_5g_agent_server_numbers.append(scala_5g_server_number)
        scala_5g_agent_delays.append(scala_5g_delay)

    _, steps_plot = plt.subplots()
    plot_steps(steps_plot, agent_steps_, agent_legacy_steps_)

    for traffic_number in range(test_traffic_quantity):
        current_traffic = tests_traffic[traffic_number]
        plot_current_traffic(traffic_number, current_traffic)
        plot_server_number(scala_5g_agent_server_numbers[traffic_number], legacy_agent_server_numbers[traffic_number],
                           current_traffic, traffic_number)
        plot_delay(scala_5g_agent_delays[traffic_number], legacy_agent_delays[traffic_number], current_traffic,
                   traffic_number)

    _, sla_plot = plt.subplots()
    plot_sla_percentage(sla_plot, scala_5g_agent_delays, legacy_agent_delays,
                        VideoStreamingEnvironment.delay_reward_upper_bound,
                        VideoStreamingEnvironment.delay_reward_lower_bound)
    plt.show()


def plot_delay(delay_scala_5g, delay_legacy_agent, test_traffic, traffic_number):
    _, delay_agent_plot = plt.subplots()
    graph_length = len(test_traffic)
    max_delay = 0.5
    min_delay = 0.2
    x = range(graph_length)
    delay_legacy_agent_plot = delay_agent_plot.twinx()
    delay_legacy_agent_plot.scatter(x, delay_legacy_agent, color=threshold_color, s=0.5)
    delay_agent_plot.scatter(x, delay_scala_5g, color=agent_color, s=0.8)
    delay_legacy_agent_plot.tick_params('y', colors=threshold_color)
    delay_agent_plot.tick_params('y', colors=agent_color)
    delay_agent_plot.set_ylabel("Delay (S)")
    delay_agent_plot.set_xlabel("Periodo de tiempo (DecaMinutos)")
    delay_agent_plot.minorticks_on()
    delay_agent_plot.set_ylim(min_delay, max_delay)
    delay_legacy_agent_plot.set_ylim(min_delay, max_delay)
    delay_agent_plot.set_title("Trafico de prueba {}".format(str(traffic_number)))


def plot_server_number(server_number_scala_5g, server_number_legacy_agent, test_traffic, traffic_number):
    _, server_number_agent_plot = plt.subplots()
    graph_length = len(test_traffic)
    x = range(graph_length)
    max_ = max(max(server_number_legacy_agent), max(server_number_scala_5g)) + 1
    server_number_legacy_agent_plot = server_number_agent_plot.twinx()
    server_number_agent_plot.minorticks_on()
    server_number_agent_plot.set_ylim(0, max_)
    server_number_legacy_agent_plot.set_ylim(0, max_)
    server_number_legacy_agent_plot.tick_params('y', colors=threshold_color)
    server_number_agent_plot.tick_params('y', colors=agent_color)
    server_number_agent_plot.plot(x, server_number_scala_5g[-graph_length:], color=agent_color)
    server_number_agent_plot.set_ylabel('Numero de Servidores - Scala5g', color=agent_color)
    server_number_legacy_agent_plot.plot(x, server_number_legacy_agent[-graph_length:], color=threshold_color)
    server_number_legacy_agent_plot.set_ylabel('Numero de Servidores - Agente Legacy', color=threshold_color)
    server_number_agent_plot.set_xlabel("Periodo de tiempo (DecaMinutos)")
    server_number_agent_plot.set_title("Trafico de prueba {}".format(str(traffic_number + 1)))


def plot_current_traffic(traffic_number, test_traffic):
    ig2, traffic_plot = plt.subplots()
    traffic_plot.scatter(range(len(test_traffic)), test_traffic, s=0.5)
    traffic_plot.minorticks_on()
    traffic_plot.set_title("Trafico de prueba {}".format(str(traffic_number + 1)))
    traffic_plot.set_ylabel("Numero de usuarios")
    traffic_plot.set_xlabel("Periodo de tiempo (DecaMinutos)")


def plot_steps(steps_plot, agent_steps, legacy_steps):
    x_label = "Episodio"
    y_label = "Numero de Pasos"
    # steps_range = min(len(agent_steps), len(legacy_steps))
    steps_plot.bar(range(len(agent_steps)), agent_steps, color=agent_color, alpha=0.5, )
    steps_plot.minorticks_on()
    steps_plot.tick_params('y', colors=agent_color)
    steps_plot.set_xlabel(x_label)
    steps_plot.set_ylabel("Scala 5G " + y_label, color=agent_color)
    steps_plot_legacy = steps_plot.twinx()
    steps_plot_legacy.bar(range(len(legacy_steps)), legacy_steps, alpha=0.5, color=threshold_color)
    steps_plot_legacy.set_ylabel("Primer Agente" + y_label, color=threshold_color)
    steps_plot_legacy.tick_params('y', colors=threshold_color)


def plot_sla_percentage(sla_plot, delay_agent, delay_legacy, upper_bound_sla, lower_bound_sla):
    x_quantity = len(delay_agent)
    x_label = []
    for i in range(x_quantity):
        x_label.append("Patron \n de Trafico {}".format(str(i + 1)))

    y_label = "Ratio de satisfacción de SLA"

    x = np.arange(len(x_label))  # the label locations
    width = 0.35  # the width of the bars
    agent_ratio = calculate_sla_ratio(delay_agent, upper_bound_sla, lower_bound_sla)
    legacy_ratio = calculate_sla_ratio(delay_legacy, upper_bound_sla, lower_bound_sla)
    rects1 = sla_plot.bar(x - width / 2, agent_ratio, width, label='Scala 5G')
    rects2 = sla_plot.bar(x + width / 2, legacy_ratio, width, label='Primer Agente')
    sla_plot.set_ylabel(y_label)
    # sla_plot.set_title('Scores by group and gender')
    sla_plot.set_xticks(x)
    sla_plot.set_xticklabels(x_label)
    sla_plot.legend()


def calculate_sla_ratio(delay_array, upper_bound, lower_bound):
    ratios = []
    for delay in delay_array:
        delay_legacy_np = np.array(delay)
        delay_legacy_sla = delay_legacy_np[(delay_legacy_np <= upper_bound) & (delay_legacy_np >= lower_bound)]
        current_ratio = len(delay_legacy_sla) / len(delay_legacy_np)
        ratios.append(current_ratio)
    return ratios
