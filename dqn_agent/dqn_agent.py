import torch
import torch.nn as nn
import torch.optim as optim
from torch import Tensor

from dqn_improvers.ExperienceReplay import ExperienceReplay
from neural_network_.NeuralNetwork import NeuralNetwork


class DeepQNetworkAgent(object):
    # TODO pass default value from hyperparameters file
    def __init__(self,
                 learning_rate,
                 memory_replay: ExperienceReplay,
                 dqn_environment,
                 nn_number_of_inputs,
                 nn_number_of_outputs,
                 nn_hidden_layer,
                 batch_size,
                 gamma,
                 update_target_frequency,
                 clip_error,
                 device=torch.device("cpu"),
                 ):
        self.update_target_frequency = update_target_frequency
        self.dqn_environment = dqn_environment
        self.memory_replay = memory_replay
        self.device = device
        self.learning_rate = learning_rate
        self.nn_number_of_inputs = nn_number_of_inputs
        self.nn_number_of_outputs = nn_number_of_outputs
        self.nn_number_of_hidden_layer = nn_hidden_layer
        self.batch_size = batch_size

        self.nn = NeuralNetwork(nn_number_of_inputs,
                                nn_hidden_layer,
                                nn_number_of_outputs).to(device)
        self.target_nn = NeuralNetwork(nn_number_of_inputs,
                                       nn_hidden_layer,
                                       nn_number_of_outputs).to(device)

        self.loss_func = nn.MSELoss()
        self.gamma = gamma

        self.optimizer = optim.Adam(params=self.nn.parameters(), lr=self.learning_rate)
        # TODO Try to use Adadelta on this optimizer.
        self.update_target_counter = 0
        self.clip_error = clip_error

    def select_action(self, state, epsilon):
        random_for_egreedy = torch.rand(1)[0]

        if random_for_egreedy > epsilon:
            with torch.no_grad():

                state = Tensor(state).to(self.device)
                action_from_nn = self.nn(state)
                action = torch.max(action_from_nn, 0)[1]
                action = action.item()
        else:
            action = self.dqn_environment.action_space.sample()

        return action

    def optimize(self, state, action, new_state, reward, done):
        # if len(self.memory_replay) < self.batch_size:
        #     return  # memory_replay without enough data
        # state, action, new_state, reward, done = self.memory_replay.sample(self.batch_size)

        # state dims: original state shape (env.state)* batch_size
        state = Tensor(state).to(self.device)
        new_state = Tensor(new_state).to(self.device)
        reward = Tensor([reward]).to(self.device)
        # action = LongTensor(action).to(self.device)
        # done = Tensor(done).to(self.device)

        new_action_from_new_state = self.target_nn(new_state).detach()
        # new_action_from_new_state.shape = torch.Size([32, 2])

        # TODO set the debugger for the cartpole, the shape has to be [cart_pole_actions]

        #  TODO given a episode number, see if each time that I execute self.target_nn(new_state)
        # a new value is given or that modify the network itself. if does not modifies it then why is detach needed
        selected_action_from_new_state = torch.max(new_action_from_new_state)
        # torch.max(new_action_from_new_state, 1)[0]  # that 1 indicates we want the max value of each row
        # that 0 indicates we want the value itself not the indices.
        if done:
            target_value = reward
        else:
            target_value = reward + self.gamma * selected_action_from_new_state
        # TODO this is reward + gamma * torch.max(Q[new_state]) you can spot that on the video 51
        # TODO minute 4:00

        # predicted_value = self.nn(state).gather(1, action.unsqueeze(1)).squeeze()  # TODO why this line?
        predicted_value = self.nn(state)[action]  # TODO why this line?
        # state.shape = torch.Size([32, 4])
        # predicted_value.shape = 32
        # predicted_value[0] = tensor(-0.3243, grad_fn=<PermuteBackward0>)
        # action.shape = 32
        # action = tensor([0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
        #         1, 0, 0, 0, 1, 0, 0, 1])

        loss = self.loss_func(predicted_value, target_value)
        # https://discuss.pytorch.org/t/what-does-the-backward-function-do/9944

        self.optimizer.zero_grad()  # Why zero grad? Cause you have to delete gradients from the previous iterations,
        # otherwise you are going to accumulate the gradient
        loss.backward()

        if self.clip_error:
            for param in self.nn.parameters():
                param.grad.data.clamp_(-1, 1)

        self.optimizer.step()

        if self.update_target_counter % self.update_target_frequency == 0:
            self.target_nn.load_state_dict(self.nn.state_dict())
        self.update_target_counter += 1
        return loss.item()


'''
update_target_frequency: 
    too high  we will not import new info into our target
    too low we update our target too often so is it will be stationary again. 

'''

'''
explanation for:  

predicted_value = self.nn(state).gather(1, action.unsqueeze(1)).squeeze()

if  action.size = [3]
    self.nn(state).size = [3, 2]
then if i use unsqueeze() on action.size:
    action.unsqueeze().size = [3, 1]
    self.nn(state).size = [3, 2]
so they are now can be multiplied.

For gather lets=
self.nn(state)      = ([4,3], [6,5], [3,9])
action.unsqueeze(1) = ([1],[0], [1])
then:
self.nn(state).gather(1, action.unsqueeze(1))
                    = ([3], [6], [9])  

For the last .squeeze(): 
self.nn(state).gather(1, action.unsqueeze(1)).shape 
             = ([3,1])
and
target_value = ([3])

so we can use something like:
        self.nn(state).gather(1, action.unsqueeze(1)).squeeze() 
             = ([3])
now they match!
'''

'''
Target Network is a copy of the LearningNN and is used to make the learning 
be as much as possible equals to supervised learning


'''

# WRT : With respect to.
