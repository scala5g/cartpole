import time

import gym
import torch
from matplotlib import pyplot as plt

from dqn_agent.dqn_agent import DeepQNetworkAgent
from dqn_improvers.ExperienceReplay import ExperienceReplay
from math_helpers.epsilon_decay import calculate_epsilon
from tune_parameters import replay_mem_size, learning_rate, batch_size, num_episodes

hidden_layer = 64
gamma = 1
update_target_frequency = 100
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")

steps_total = []

frames_total = 0
solved_after = 0
solved = False
score_to_solve = 195
start_time = time.time()
report_interval = 10
rewards = []
# TODO add dropout.

if __name__ == "__main__":
    memory_replay = ExperienceReplay(replay_mem_size)
    env = gym.make('CartPole-v0')
    # this place is where it is appended the TimeLimit class, it is responsible for limit the episode, i.e the max episodes
    # for cartpole are 200, to modify it just change:
    # env.spec.max_episode_steps
    # when the step inside cartpole returns, the TimeLimit class calls the step function on itself and check  the
    # current step if is greater than the max_episode_steps then done = true.

    number_of_inputs = env.observation_space.shape[0]
    number_of_outputs = env.action_space.n
    dqn_agent = DeepQNetworkAgent(learning_rate,
                                  memory_replay,
                                  env,
                                  number_of_inputs,
                                  number_of_outputs,
                                  hidden_layer,
                                  batch_size,
                                  gamma,
                                  update_target_frequency,
                                  clip_error=False,
                                  device=device
                                  )
    for episode in range(num_episodes):
        state = env.reset()
        step = 0
        while True:
            step += 1
            frames_total += 1
            epsilon = calculate_epsilon(frames_total)
            action = dqn_agent.select_action(state, epsilon)
            new_state, reward, done, info = env.step(action)
            memory_replay.push(state, action, new_state, reward, done)
            dqn_agent.optimize()
            state = new_state
            if done:
                steps_total.append(step)

                mean_reward_100 = sum(steps_total[-100:]) / 100
                rewards.append(mean_reward_100)
                if mean_reward_100 > score_to_solve and solved == False:
                    print("SOLVED! After %i episodes " % episode)
                    solved_after = episode
                    solved = True

                if episode % report_interval == 0:
                    print("\n*** Episode %i *** \
                                     \nAv.reward: [last %i]: %.2f, [last 100]: %.2f, [all]: %.2f \
                                     \nepsilon: %.2f, frames_total: %i"
                          %
                          (episode,
                           report_interval,
                           sum(steps_total[-report_interval:]) / report_interval,
                           mean_reward_100,
                           sum(steps_total) / len(steps_total),
                           epsilon,
                           frames_total
                           )
                          )

                    elapsed_time = time.time() - start_time
                    print("Elapsed time: ", time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
                break

# loss = list(list(zip(*loss_val)))
# y = loss[0]
# x = [x[0] for x in loss[1]]
x = range(len(rewards))
y = rewards
plt.scatter(x, y, alpha=0.6, color='green')
plt.show()
