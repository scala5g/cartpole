import time

import torch

from dqn_5g_environment.VideoStreamingEnvironment import VideoStreamingEnvironment, generate_traffic
from dqn_agent.dqn_agent import DeepQNetworkAgent
from dqn_improvers.ExperienceReplay import ExperienceReplay
from math_helpers.epsilon_decay import calculate_epsilon
from plotting.dqn_plot import plot_video_server_dqn_behaviour
from tune_parameters import replay_mem_size, learning_rate, num_episodes

hidden_layer = 64 * 2
gamma = 0.9
update_target_frequency = 100
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
steps_total = []

frames_total = 0
solved_after = 0
solved = False
score_to_solve = 140
start_time = time.time()
report_interval = 10
rewards = []
# TODO add dropout.
delay = []
env_reward = []
loss_ = []
agent_mean_loss_per_episode = []
min_episodes = 2000

# TODO check for overlapping intervals.

env = VideoStreamingEnvironment()
max_steps = len(generate_traffic())


def extra_bonus(step, max_steps, done, reward):
    if done:
        reward_steps_bonus = 65 if step > (max_steps - 3) else 0
        reward += reward_steps_bonus
    return reward


def solved_condition(av_steps_from_report_window, max_steps):
    return av_steps_from_report_window > (max_steps - 5)


def get_average_steps_from_report_window(steps_total, steps_window):
    return sum(steps_total[-steps_window:]) / steps_window


def get_episode_reward(env_reward, step):
    return sum(env_reward[-step:]) / step


def get_steps_window(report_interval, margin):
    return report_interval + margin


def train_network(env, dqn_agent, num_episodes, frames_total, min_episodes, solve_without_minimum_episodes=False):
    environment_solved = False
    for episode in range(num_episodes):
        state = env.reset()
        step = 0
        while True:
            step += 1
            frames_total += 1
            epsilon = calculate_epsilon(frames_total)
            action = dqn_agent.select_action(state, epsilon)
            new_state, reward, done, info = env.step(action)
            # memory_replay.push(state, action, new_state, reward, done)
            delay.append(new_state[0])
            extra_bonus(step, max_steps, done, reward)
            env_reward.append(reward)
            loss = dqn_agent.optimize(state, action, new_state, reward, done)
            loss_.append(loss)
            state = new_state
            if done:
                steps_total.append(step)
                episode_reward = get_episode_reward(env_reward, step)
                rewards.append(episode_reward)
                steps_window = get_steps_window(report_interval, margin=4)
                av_steps_from_report_window = get_average_steps_from_report_window(steps_total, steps_window)
                if solved_condition(av_steps_from_report_window, max_steps) and environment_solved is False:
                    print("SOLVED! After %i episodes " % episode)
                    environment_solved = True

                if episode % report_interval == 0:
                    agent_mean_loss_on_this_episode = sum(loss_[-step:]) / step
                    agent_mean_loss_per_episode.append(agent_mean_loss_on_this_episode)
                    print("\n*** Episode %i *** \
                                     \nAv.steps: [last %i episodes]: %.2f, [episode_reward]: %.2f, [all]: %.2f \
                                     \nepsilon: %.2f, frames_total: %i, steps: %i"
                          % (episode, report_interval, av_steps_from_report_window, episode_reward,
                             agent_mean_loss_on_this_episode, epsilon, frames_total, step))

                    elapsed_time = time.time() - start_time
                    print("Elapsed time: ", time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
                break
        if environment_solved and (episode > min_episodes or solve_without_minimum_episodes):
            print("solved! Breaking the loop!")
            break


def main(plot=True, solve_without_minimum_episodes=False):
    memory_replay = ExperienceReplay(replay_mem_size)
    # this place is where it is appended the TimeLimit class, it is responsible for limit the episode,
    # i.e the max episodes for cartpole are 200, to modify it just change:
    # env.spec.max_episode_steps
    # when the step inside cartpole returns, the TimeLimit class calls the step function on itself and check  the
    # current step if is greater than the max_episode_steps then done = true.
    number_of_inputs = env.observation_space.shape[0]
    number_of_outputs = env.action_space.n
    dqn_agent = DeepQNetworkAgent(learning_rate, memory_replay, env, number_of_inputs, number_of_outputs, hidden_layer,
                                  0,  # Batch_size
                                  gamma, update_target_frequency, clip_error=False, device=device)
    train_network(env, dqn_agent, num_episodes, frames_total, min_episodes,
                  solve_without_minimum_episodes=solve_without_minimum_episodes)
    print("AV steps: %.2f " % (sum(steps_total) / len(steps_total)))
    print("AV reward: %.2f " % (sum(env_reward) / len(env_reward)))
    if plot:
        plot_video_server_dqn_behaviour(env, dqn_agent, steps_total, graph_previous_agents=True)
    return steps_total


if __name__ == "__main__":
    main(solve_without_minimum_episodes=True)
