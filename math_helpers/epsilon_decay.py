import math

from tune_parameters import egreedy_final, egreedy_decay, egreedy


def calculate_epsilon(steps_done):
    return egreedy_final + (egreedy - egreedy_final) * \
            math.exp(-1. * steps_done / egreedy_decay)
