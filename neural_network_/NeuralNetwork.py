import torch.nn as nn


class NeuralNetwork(nn.Module):
    def __init__(self, number_of_inputs, hidden_layer, number_of_outputs):
        super(NeuralNetwork, self).__init__()
        self.linear1 = nn.Linear(number_of_inputs, hidden_layer)
        self.linear2 = nn.Linear(hidden_layer, number_of_outputs)

        self.activation = nn.Tanh()
        # self.activation = nn.ReLU()

    def forward(self, network_input):
        output1 = self.linear1(network_input)
        output1 = self.activation(output1)
        output2 = self.linear2(output1)
        return output2


'''
Activation function: Activate/deactivate depending on if data is relevant or not
Loss Function: How good is performing the net, difference between real/predicted
Optimizers: To minimize the loss, try to find the parameters to leave the loss function at the lowest.
 
'''
