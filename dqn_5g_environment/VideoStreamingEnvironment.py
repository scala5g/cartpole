import random

import gym
import numpy as np
from gym import spaces
from gym.utils import seeding


def generate_traffic_(traffic_length=np.arange(0, 24 * 2, 0.166)):
    switch_traffic = {
        1: 30 * np.sin(traffic_length / 3) + 40,
        2: 30 * np.cos(traffic_length / 2) + 40,
        3: (15 * np.cos(traffic_length / 5) + 15 * np.sin(traffic_length / 5) + 40),
        4: (15 * np.cos(traffic_length / 2) + 15 * np.sin(traffic_length / 5) + 40),
        5: 15 * np.cos(traffic_length / 5) + 15 * np.cos(traffic_length / 3) + 40
    }
    return switch_traffic.get(random.randint(1, 5))


def generate_test_traffic(traffic_length=np.arange(0, 24 * 2, 0.166)):
    # return 30 * np.sin(traffic_length / 4) + 40
    return 150 + 100 * np.sin(traffic_length) + 2 * np.sin(3 * traffic_length) + 0.5 * np.sin(traffic_length)
    # return 35 * np.cos(3.14 / 2 + traffic_length / 2) + 23 * np.sin(traffic_length / 3) + 80


# return np.floor(30 * np.sin(traffic_length / 5) + 70 * np.sin(traffic_length / 2) + 120)

def generate_traffic(traffic_length=np.arange(0, 24 * 2, 0.166)):
    # return np.sin(traffic_length / 4) * 1 / 2 + 1 / 2
    first_ = random.randint(2, 11)
    second_ = random.randint(2, 11)

    first_amplitude = random.randint(0, 90)
    second_amplitude = random.randint(0, 90)

    bias = random.randint(0, 4)

    amplitude = first_amplitude + second_amplitude + 20

    return np.abs(first_amplitude * np.sin(traffic_length / first_)
                  + second_amplitude * np.sin(bias + traffic_length / second_)
                  + amplitude)


# return np.floor(8 * np.cos(traffic_length / 7) + 20 * np.cos(traffic_length / 2) + 40)
# return np.floor(20 * np.cos(traffic_length / 3) + 15 * np.sin(traffic_length / 2) + 40)
# return np.floor(10*np.cos(traffic_length)) + 10

'''
0: increase the number of servers 
1: maintain the number of servers
2: decrease the number of servers 
'''


def _reward_function(current_delay):
    # 320 -> 31 users
    # 284 -> 10
    server_delay_reward = 20 if (VideoStreamingEnvironment.delay_reward_upper_bound
                                 > current_delay >
                                 VideoStreamingEnvironment.delay_reward_lower_bound) else 0
    # server_delay_reward = np.exp(-0.0015 * np.power(current_delay - 310, 2))
    return server_delay_reward


# TODO
#       The cartpole init on perfect balance (delay goal) and if he goes to the left (scale_up)
#       at some point he have to go right (scale_down) to balance the pole... what if the env
#       starts in a perfect balance? and scale down is move to right and scale up is move to left..?

class VideoStreamingEnvironment(gym.Env):
    metadata = {'render.modes': ['human']}
    initial_number_of_servers = 1
    max_number_of_servers = 6
    scale_up_servers = 0
    maintain_server_number = 1
    scale_down_servers = 2
    delay_reward_upper_bound = 0.320
    delay_reward_lower_bound = 0.200

    # Delay in milliseconds
    def __init__(self):
        super(VideoStreamingEnvironment, self).__init__()
        self.max_delay_in_s = 1
        self.min_delay_in_s = 0
        self.max_number_of_servers = VideoStreamingEnvironment.max_number_of_servers
        self.min_number_of_servers = 1
        self.max_number_of_users = 200
        self.min_number_of_users = 0
        low = np.array([self.min_delay_in_s,
                        self.min_number_of_servers,
                        # self.min_number_of_users,
                        ], dtype=np.float32)
        high = np.array([self.max_delay_in_s,
                         self.max_number_of_servers,
                         # self.max_number_of_users,
                         ], dtype=np.float32)
        self.action_space = spaces.Discrete(3)
        self.observation_space = spaces.Box(low=low, high=high, dtype=np.float32)
        self.wrong_server_scale_penalty = -0.5
        self.server_cost_penalty = -0.5
        self.previous_action_was_scale = False
        self.traffic_pool = []
        # for _ in range(2000):
        #     self.traffic_pool.append(generate_traffic())
        self._initEnvironment()

    def get_traffic_from_traffic_pool(self):
        return self.traffic_pool[random.randint(0, len(self.traffic_pool) - 1)]

    def _initEnvironment(self):
        self.seed()
        # self.traffic = self.get_traffic_from_traffic_pool()
        self.traffic = generate_traffic()
        self.current_step = 0
        self.state = None
        self.current_number_of_servers = VideoStreamingEnvironment.initial_number_of_servers
        self.transition_threshold = 0
        self.current_transition = 0
        self.rewards = []

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        self._take_action(action)
        current_number_of_users = self.traffic[self.current_step]  # TODO test the index out bound.
        delay = self._server_delay(current_number_of_users)  # TODO limit the delay for high values.
        self.state = (delay, self.current_number_of_servers)
        self.current_step += 1
        reward = _reward_function(delay)
        self.rewards.append(reward)
        done = self.current_step == len(self.traffic)
        if not done and len(self.rewards) > 20:
            # if (sum(self.rewards[-10:]) / 10) < 0.3:
            if delay >= 0.4 or delay < 0.15:
                # if delay >= 0.49 or delay < 0.1:
                reward = 0
                done = True
            else:
                reward += 0.2
                done = False
        if self.previous_action_was_scale and \
                (action != VideoStreamingEnvironment.maintain_server_number):
            # reward *= 0.9
            reward *= 0.98
        if action == VideoStreamingEnvironment.maintain_server_number:
            reward *= 1.81
        # TODO create a reward when the previous delay is steady :) in fact that could be the only reward fun

        self.previous_action_was_scale = False if action == VideoStreamingEnvironment.maintain_server_number else True

        return np.array(self.state, dtype=np.float32), reward, done, current_number_of_users

    def _take_action(self, action):
        err_msg = "%r (%s) invalid" % (action, type(action))
        assert self.action_space.contains(action), err_msg
        # Simulate the delay between the server number transition
        if self.current_transition is not 0:
            self.current_transition -= 1
            return

        # TODO unit test this...
        if action == VideoStreamingEnvironment.scale_up_servers \
                and self.current_number_of_servers < self.max_number_of_servers:
            self.current_number_of_servers += 1
            self.current_transition = self.transition_threshold
        elif action == VideoStreamingEnvironment.scale_down_servers \
                and self.current_number_of_servers >= 2:
            self.current_number_of_servers -= 1
            self.current_transition = self.transition_threshold

        # if action == 1 and self.current_number_of_servers >= self.max_number_of_servers:
        #     return self.wrong_server_scale_penalty
        # elif action == 2 and self.current_number_of_servers < 2:
        #     return self.wrong_server_scale_penalty
        # else:
        #     return 0

    def _server_delay(self, total_users):
        users_per_server = total_users / self.current_number_of_servers
        # delay = 0.34 + (np.exp(-1.2 + users_per_server / 60) - np.exp(- (50 + users_per_server) / 200)) / 2
        # f(x) = ((ℯ ^ (-1.2 + ((x) / (6z0))) - ℯ ^ (-((50 + x) / (200)))) / (2)) + 0.34
        delay = 300 + (0.4 * np.exp(-1 + (users_per_server + 4) / 7.1) - np.exp(6 - users_per_server / 25)) / 2
        # delay = 300 + (4 * np.exp(-1 + (users_per_server + 2) / 10) - np.exp(6 - users_per_server / 25)) / 2
        delay = delay / 1000
        return np.clip(delay, self.min_delay_in_s, self.max_delay_in_s)

    def _server_delay_(self, total_users):
        users_per_server = total_users / self.current_number_of_servers
        # delay = 300 + (0.4 * np.exp(-1 + (users_per_server + 4) / 6) - np.exp(4.5 - users_per_server / 10)) / 2
        # if delay > 10000:
        #     return 10000
        # else:
        #     return delay
        return 0.005 * users_per_server + 0.1

    def reset(self):
        self._initEnvironment()
        current_number_of_users = self.traffic[self.current_step]
        delay = self._server_delay(current_number_of_users)
        self.state = (delay, self.current_number_of_servers,
                      # current_number_of_users
                      )
        return np.array(self.state, dtype=np.float32)

    def render(self, mode="human"):
        pass
