import random


class ExperienceReplay(object): # TODO depend on abstraction not implementations ;)
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, state, action, new_state, reward, done):
        current_batch = (state, action, new_state, reward, done)

        current_position = self.position
        if current_position >= len(self.memory):
            self.memory.append(current_batch)
        else:
            self.memory[current_position] = current_batch
        self.position = (current_position + 1) % self.capacity

    def sample(self, batch_size):
        return zip(*random.sample(self.memory, batch_size))

    def __len__(self):
        return len(self.memory)
