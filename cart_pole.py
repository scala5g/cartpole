import torch
from torch._C import device
import torch.nn as nn
import torch.optim as optim
import gym
import random
import math

from tune_parameters import seed_value


class CartPole:
    device

    def __init__(self):
        use_cuda: bool = torch.cuda.is_available()
        self.device = torch.device("cuda:0" if use_cuda else "cpu")
        self.Tensor = torch.Tensor
        self.env = gym.make('CartPole-v0')
        self.env.seed(seed_value)
        torch.manual_seed(seed_value)
        random.seed(seed_value)


