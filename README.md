# Welcome to SCALA 5G repository.
On this repository resides the SCALA 5G - DQN Agent. It solves a scaling problem for an eMBB 5G use case. The gym environment is extended from OpenAI-gym and as the `CartPole` our Agent can take 3 actions:
- Scale up the number of servers.
- Scale down the number of servers.
- Maintain the number of servers.

The `CartPole` name on the URL is to honor the first environment that we can solve by ourselves alone.

You can train the agent executing the file:


`dqn_training/train_video_server_dqn.py`
#### Code Coverage
coverage run --source=./ -m unittest discover -s tests/ && coverage html

#### Unit tests
python3 -m unittest discover -s tests
