import random

import gym
import numpy as np
from gym import spaces
from gym.utils import seeding


def generate_traffic(traffic_length=np.arange(0, 24 * 1, 0.166)):
    switch_traffic = {
        1: 30 * np.sin(traffic_length / 3) + 40,
        2: 30 * np.cos(traffic_length / 2) + 40,
        3: (15 * np.cos(traffic_length / 5) + 15 * np.sin(traffic_length / 5) + 40),
        4: (15 * np.cos(traffic_length / 2) + 15 * np.sin(traffic_length / 5) + 40),
        5: 15 * np.cos(traffic_length / 5) + 15 * np.cos(traffic_length / 3) + 40
    }
    return switch_traffic.get(random.randint(1, 5))


# return np.floor(30 * np.sin(traffic_length / 5) + 70 * np.sin(traffic_length / 2) + 120)

def generate_traffic_(traffic_length=np.arange(0, 24 * 1, 0.166)):
    # return np.sin(traffic_length / 4) * 1 / 2 + 1 / 2
    first_ = random.randint(2, 11)
    second_ = random.randint(2, 11)

    first_amplitude = random.randint(20, 60)
    second_amplitude = random.randint(20, 60)

    bias = random.randint(0, 4)

    amplitude = first_amplitude + second_amplitude + 5

    return np.abs(first_amplitude * np.sin(traffic_length / first_)
                  + second_amplitude * np.sin(bias + traffic_length / second_)
                  + amplitude)


# return np.floor(8 * np.cos(traffic_length / 7) + 20 * np.cos(traffic_length / 2) + 40)
# return np.floor(20 * np.cos(traffic_length / 3) + 15 * np.sin(traffic_length / 2) + 40)
# return np.floor(10*np.cos(traffic_length)) + 10

'''
0: increase the number of servers 
1: maintain the number of servers
2: decrease the number of servers 
'''


def _reward_function(current_delay):
    # 320 -> 31 users
    # 284 -> 10
    server_delay_reward = 1 if (0.320 > current_delay > 0.15) else 0
    # server_delay_reward = np.exp(-0.0015 * np.power(current_delay - 310, 2))
    return server_delay_reward


# TODO
#       The cartpole init on perfect balance (delay goal) and if he goes to the left (scale_up)
#       at some point he have to go right (scale_down) to balance the pole... what if the env
#       starts in a perfect balance? and scale down is move to right and scale up is move to left..?

class VideoStreamingEnvironment(gym.Env):
    metadata = {'render.modes': ['human']}
    initial_number_of_servers = 1
    max_number_of_servers = 6
    scale_up_servers = 0
    maintain_server_number = 1
    scale_down_servers = 2

    # Delay in milliseconds
    def __init__(self):
        super(VideoStreamingEnvironment, self).__init__()
        max_delay = 10000
        min_delay = 100
        self.max_number_of_servers = VideoStreamingEnvironment.max_number_of_servers
        self.min_number_of_servers = 1
        self.max_number_of_users = 200
        self.min_number_of_users = 0
        low = np.array([min_delay,
                        self.min_number_of_servers,
                        # self.min_number_of_users,
                        ], dtype=np.float32)
        high = np.array([max_delay,
                         self.max_number_of_servers,
                         # self.max_number_of_users,
                         ], dtype=np.float32)
        self.action_space = spaces.Discrete(3)
        self.observation_space = spaces.Box(low=low, high=high, dtype=np.float32)
        self.wrong_server_scale_penalty = -0.5
        self.server_cost_penalty = -0.5
        self._initEnvironment()

    def _initEnvironment(self):
        self.seed()
        self.traffic = generate_traffic()
        self.current_step = 0
        self.state = None
        self.current_number_of_servers = VideoStreamingEnvironment.initial_number_of_servers
        self.transition_threshold = 0
        self.current_transition = 0
        self.rewards = []

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        self._take_action(action)
        current_number_of_users = self.traffic[self.current_step]  # TODO test the index out bound.
        delay = self._server_delay(current_number_of_users)  # TODO limit the delay for high values.
        self.state = (delay, self.current_number_of_servers)
        self.current_step += 1
        reward = _reward_function(delay)
        self.rewards.append(reward)
        done = self.current_step == len(self.traffic)
        if not done and len(self.rewards) > 20:
            # if (sum(self.rewards[-10:]) / 10) < 0.3:
            if delay >= 0.7 or delay < 0.15:
                reward = 0
                done = True
            else:
                reward += 0.2
                done = False

        return np.array(self.state, dtype=np.float32), reward, done, current_number_of_users

    def _take_action(self, action):
        err_msg = "%r (%s) invalid" % (action, type(action))
        assert self.action_space.contains(action), err_msg
        # Simulate the delay between the server number transition
        if self.current_transition is not 0:
            self.current_transition -= 1
            return

        # TODO unit test this...
        if action == VideoStreamingEnvironment.scale_up_servers \
                and self.current_number_of_servers < self.max_number_of_servers:
            self.current_number_of_servers += 1
            self.current_transition = self.transition_threshold
        elif action == VideoStreamingEnvironment.scale_down_servers \
                and self.current_number_of_servers >= 2:
            self.current_number_of_servers -= 1
            self.current_transition = self.transition_threshold

        # if action == 1 and self.current_number_of_servers >= self.max_number_of_servers:
        #     return self.wrong_server_scale_penalty
        # elif action == 2 and self.current_number_of_servers < 2:
        #     return self.wrong_server_scale_penalty
        # else:
        #     return 0

    def _server_delay(self, total_users):
        users_per_server = total_users / self.current_number_of_servers
        # delay = 300 + (0.4 * np.exp(-1 + (users_per_server + 4) / 6) - np.exp(4.5 - users_per_server / 10)) / 2
        # if delay > 10000:
        #     return 10000
        # else:
        #     return delay
        return 0.005 * users_per_server + 0.1

    def reset(self):
        self._initEnvironment()
        current_number_of_users = self.traffic[self.current_step]
        delay = self._server_delay(current_number_of_users)
        self.state = (delay, self.current_number_of_servers,
                      # current_number_of_users
                      )
        return np.array(self.state, dtype=np.float32)

    def render(self, mode="human"):
        pass
