import time

import torch
from torch import Tensor, nn, optim

from dqn_improvers.ExperienceReplay import ExperienceReplay
from legacy.video_streaming_legacy import VideoStreamingEnvironment
from math_helpers.epsilon_decay import calculate_epsilon
from tune_parameters import replay_mem_size, learning_rate

hidden_layer = 64
gamma = 0.9
update_target_frequency = 100
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")


solved_after = 0
score_to_solve = 140
start_time = time.time()
report_interval = 10
rewards = []
# TODO add dropout.
server_number = []
delay = []
users = []
env_reward = []
action_taken = []
loss_ = []


# TODO overlapping intervals.
# what if i made the reward similar to the cartpole env
class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.linear1 = nn.Linear(number_of_inputs, hidden_layer)
        self.linear2 = nn.Linear(hidden_layer, number_of_outputs)

        self.activation = nn.Tanh()
        # self.activation = nn.ReLU()

    def forward(self, x):
        output1 = self.linear1(x)
        output1 = self.activation(output1)
        output2 = self.linear2(output1)

        return output2


class QNet_Agent(object):
    def __init__(self):
        self.nn = NeuralNetwork().to(device)

        self.loss_func = nn.MSELoss()
        # self.loss_func = nn.SmoothL1Loss()

        self.optimizer = optim.Adam(params=self.nn.parameters(), lr=learning_rate)
        # self.optimizer = optim.RMSprop(params=mynn.parameters(), lr=learning_rate)

    def select_action(self, state, epsilon):

        random_for_egreedy = torch.rand(1)[0]

        if random_for_egreedy > epsilon:

            with torch.no_grad():

                state = Tensor(state).to(device)
                action_from_nn = self.nn(state)
                action = torch.max(action_from_nn, 0)[1]
                action = action.item()
        else:
            action = env.action_space.sample()

        return action

    def optimize(self, state, action, new_state, reward, done):

        state = Tensor(state).to(device)
        new_state = Tensor(new_state).to(device)

        reward = Tensor([reward]).to(device)

        if done:
            target_value = reward
        else:
            new_state_values = self.nn(new_state).detach()
            max_new_state_values = torch.max(new_state_values)
            target_value = reward + gamma * max_new_state_values  # TODO Add target net

        predicted_value = self.nn(state)[action]

        loss = self.loss_func(predicted_value, target_value)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        return loss.item()


env = VideoStreamingEnvironment()
memory_replay = ExperienceReplay(replay_mem_size)
number_of_inputs = env.observation_space.shape[0]
number_of_outputs = env.action_space.n


def train_legacy_agent():
    frames_total = 0
    dqn_agent = QNet_Agent()
    solved = False
    num_episodes = 1000
    steps_total = []
    for episode in range(num_episodes):
        state = env.reset()
        step = 0
        while True:
            step += 1
            frames_total += 1
            # print("step" + str(step))
            epsilon = calculate_epsilon(frames_total)
            action = dqn_agent.select_action(state, epsilon)
            new_state, reward, done, info = env.step(action)
            # memory_replay.push(state, action, new_state, reward, done)
            delay.append(new_state[0])
            users.append(info)
            server_number.append(new_state[1])
            action_taken.append(action)
            if done:
                reward_steps_penalty = 30 if step > 140 else 0
                reward += reward_steps_penalty
            env_reward.append(reward)
            loss = dqn_agent.optimize(state, action, new_state, reward, done)
            loss_.append(loss)
            state = new_state
            if done:
                steps_total.append(step)

                # mean_reward_100 = sum(steps_total[-100:]) / 100
                mean_reward_100 = sum(env_reward[step:]) / step
                rewards.append(mean_reward_100)
                if mean_reward_100 > score_to_solve and solved == False:
                    print("SOLVED! After %i episodes " % episode)
                    solved_after = episode
                    solved = True

                if episode % report_interval == 0:
                    print("\n*** Episode %i *** \
                                     \nAv.reward: [last %i]: %.2f, [last 100]: %.2f, [all]: %.2f \
                                     \nepsilon: %.2f, frames_total: %i, steps: %i"
                          %
                          (episode,
                           report_interval,
                           sum(env_reward[-report_interval:]) / report_interval,
                           mean_reward_100,
                           sum(env_reward) / len(env_reward),
                           epsilon,
                           frames_total,
                           step
                           )
                          )

                    elapsed_time = time.time() - start_time
                    print("Elapsed time: ", time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
                break
    return dqn_agent, steps_total
