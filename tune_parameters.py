###### PARAMS ######
learning_rate = 0.007
num_episodes = 200000
gamma = 0.9

egreedy = 0.9
egreedy_final = 0.02
egreedy_decay = 10000
seed_value = 23

replay_mem_size = 100
batch_size = 32
